# Copyright (c) 2022, Beshoy Atef and contributors
# For license information, please see license.txt

import frappe
from frappe.model.document import Document
import requests

import json
class Courier(Document):
	def validate(self):
		#get maped api 
		staus_map = {}
		if self.status_template :
			for stat in self.status_template:
				staus_map[stat.company_staus] =stat.waybill_status

		if self.api__maping_template :
			for api_type in self.api__maping_template :
				# frappe.msgprint( str(api_type))
				api_tem = frappe.get_doc("Api Template" , api_type.template)
				# frappe.msgprint(str(api_tem.api_type))

				json_temlate = api_tem.api_col_template 
				document_map_json = {}
				data_build = []
				for k  in json_temlate :
					document_map_json[k.col_name] = k.api_key
				# frappe.msgprint(str(document_map_json))
				if api_tem.api_type == "Receive":
						url = api_tem.posting_url 
					
					# try :
						if not api_tem.login_url :
							request = requests.get(url)
							json_data = []
							if request.status_code != 200 :
								pass
								frappe.throw("error")
							json_data = json.loads(request.text)
						if api_tem.login_url :
								# frappe.throw("Catch")
								#1- get token
								login_url = api_tem.login_url
								username = api_tem.api_key
								password= api_tem.api_secret
								header ={"Content-Type": "application/json"}
								req_login = requests.post(login_url , headers =header ,
								 data=json.dumps({"username":username ,"password" :password}))
								json_reponse = json.loads(req_login.text)
								token = str(json_reponse.get("access"))
								if token :
									header ={api_tem.authorization_method : "%s  %s"%(api_tem.authorization_key , token)}
									request = requests.get(url , headers=header )
									# frappe.throw(str(request.text))
									json_data = json.loads(request.text)
							
						
						for i in json_data :
							data_object= {}
							for k ,v in document_map_json.items() :
								
								data_object[k] = i.get(v)
							data_build.append(data_object)
							for obj in data_build :
								if obj.get("remote_id"):
									new_order = self.check_exit_waybill(obj.get("remote_id") ,self.name)
									if new_order :
										new_order_obj = frappe.new_doc("Waybill")
										new_order_obj.posting_data = obj.get("posting_data").split("T")[0] or None
										new_order_obj.company = self.name
										new_order_obj.remote_id = obj.get("remote_id") 
										new_order_obj.customer_name = obj.get("customer_name") or " "
										new_order_obj.customer_contact_number = obj.get("customer_contact_number") or " "
										new_order_obj.customer_contact_2 = obj.get("customer_contact_2") or " "
										new_order_obj.country = obj.get("country") or " "
										new_order_obj.city = obj.get("city") or " "
										new_order_obj.area = obj.get("area") or " "
										new_order_obj.street_name = obj.get("street_name") or " "
										new_order_obj.building_number = obj.get("building_number") or " "
										new_order_obj.address_line_1 = obj.get("address_line_1") or " "
										new_order_obj.adress_line2 = obj.get("adress_line2") or " "
										new_order_obj.delivery_fees = obj.get("delivery_fees") or " "
										new_order_obj.order_fees = obj.get("order_fees") or " "
										new_order_obj.total_fees = obj.get("total_fees") or " "
										new_order_obj.waybill_status = staus_map.get(obj.get("waybill_status"))
										
										new_order_obj.save()
							frappe.msgprint((str(data_build)))
					# except :pass
							
						# frappe.msgprint(str(request.text))




	def check_exit_waybill(self , billid , courier):
		bill = frappe.db.sql("SELECT name FROM `tabWaybill` WHERE remote_id ='%s' and company='%s' "%(billid , courier ),as_dict =1 )
		if len(bill) > 0 :
			return False
		else :
			return True

