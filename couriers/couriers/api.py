import frappe
import requests
import json

@frappe.whitelist()
def create_er(*args , **kwargs):
    e =frappe.new_doc("Error Log")
    e.error = "Created"
    e.save()
    return 1



def check_exit_waybill( billid , courier):
		bill = frappe.db.sql("SELECT name FROM `tabWaybill` WHERE remote_id ='%s' and company='%s' "%(billid , courier ),as_dict =1 )
		if len(bill) > 0 :
			return False
		else :
			return True
@frappe.whitelist()
def update_api(*args ,**kwargs):
    
    couriers = frappe.db.sql(""" SELECT name FROM `tabCourier` """ , as_dict =1)
   
    for courier in couriers :
        self = frappe.get_doc("Courier" , courier.get("name"))
        
        staus_map = {}
       
        if self.status_template :
           
            for stat in self.status_template:
                staus_map[stat.company_staus] =stat.waybill_status
        if self.api__maping_template :
            e =frappe.new_doc("Error Log")
            e.error = "El Bat2"
            e.save()
            for api_type in self.api__maping_template :
                
                # frappe.msgprint( str(api_type))
                api_tem = frappe.get_doc("Api Template" , api_type.template)
                # frappe.msgprint(str(api_tem.api_type))

                json_temlate = api_tem.api_col_template 
                document_map_json = {}
                data_build = []
                for k  in json_temlate :
                    document_map_json[k.col_name] = k.api_key
                # frappe.msgprint(str(document_map_json))
                
                json_data = []
                url = api_tem.posting_url 
                if not api_tem.login_url :
                    request = requests.get(url)
                    
                    if request.status_code != 200 :
                        pass
                        
                    json_data = json.loads(request.text)
                if api_tem.login_url :
                       
                        # frappe.throw("Catch")
                        #1- get token
                        login_url = api_tem.login_url
                        username = api_tem.api_key
                        password= api_tem.api_secret
                        header ={"Content-Type": "application/json"}
                        req_login = requests.post(login_url , headers =header ,
                            data=json.dumps({"username":username ,"password" :password}))
                        json_reponse = json.loads(req_login.text)
                        token = str(json_reponse.get("access"))
                        if token :
                            header ={api_tem.authorization_method : "%s  %s"%(api_tem.authorization_key , token)}
                            request = requests.get(url , headers=header )
                            # frappe.throw(str(request.text))
                            json_data = json.loads(request.text)
                    
                        for i in json_data :
                            data_object= {}
                            for k ,v in document_map_json.items() :
                                
                                data_object[k] = i.get(v)
                            data_build.append(data_object)
                            if api_tem.api_type == "Receive":
                                for obj in data_build :
                                    if obj.get("remote_id"):
                                        
                                        new_order = check_exit_waybill(obj.get("remote_id") ,self.name)
                                        if new_order :
                                            new_order_obj = frappe.new_doc("Waybill")
                                            new_order_obj.posting_data = obj.get("posting_data").split("T")[0] or None
                                            new_order_obj.company = self.name
                                            new_order_obj.remote_id = obj.get("remote_id") 
                                            new_order_obj.customer_name = obj.get("customer_name") or " "
                                            new_order_obj.customer_contact_number = obj.get("customer_contact_number") or " "
                                            new_order_obj.customer_contact_2 = obj.get("customer_contact_2") or " "
                                            new_order_obj.country = obj.get("country") or " "
                                            new_order_obj.city = obj.get("city") or " "
                                            new_order_obj.area = obj.get("area") or " "
                                            new_order_obj.street_name = obj.get("street_name") or " "
                                            new_order_obj.building_number = obj.get("building_number") or " "
                                            new_order_obj.address_line_1 = obj.get("address_line_1") or " "
                                            new_order_obj.adress_line2 = obj.get("adress_line2") or " "
                                            new_order_obj.delivery_fees = obj.get("delivery_fees") or " "
                                            new_order_obj.order_fees = obj.get("order_fees") or " "
                                            new_order_obj.total_fees = obj.get("total_fees") or " "
                                            new_order_obj.waybill_status = staus_map.get(obj.get("waybill_status"))
                                            
                                            new_order_obj.save()
                            if api_tem.api_type == "Update Status":
                                
                                for obj in data_build :
                                    if obj.get("remote_id"): 
                                        new_order = check_exit_waybill(obj.get("remote_id") ,self.name)
                                        if not new_order :
                                            e =frappe.new_doc("Error Log")
                                            e.error = str(staus_map.get(obj.get("waybill_status")))
                                            e.save()  
                                            frappe.db.sql(""" UPDATE `tabWaybill`  SET waybill_status ='%s' WHERE remote_id ='%s' """ %(obj.get("waybill_status"),obj.get("remote_id")))
                                            frappe.db.commit()
                                            



                                 
                            frappe.msgprint((str(data_build)))	

@frappe.whitelist()
def sync_now(*args ,**kwargs):
    from frappe.utils.background_jobs import enqueue
    enqueue('couriers.couriers.api.update_api', timeout=2000, queue="long")
